from setuptools import setup

PACKAGE = "tks"

setup(name=PACKAGE, packages=[PACKAGE], test_suite="tks")
