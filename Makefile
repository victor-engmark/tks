NAME = tks

docker_tag = tks
docker_run = docker run --volume $(CURDIR):/usr/src/app $(docker_tag)

all: build

clean:
	fakeroot make -f debian/rules clean
	rm -f doc/spec/*.html

build:
	dpkg-buildpackage -rfakeroot -us -uc -b -tc

debug:
	dpkg-buildpackage -rfakeroot -us -uc -b

docs:
	rst2html doc/spec/tks-functional-spec.rst doc/spec/tks-functional-spec.html

.PHONY: test
test: container
	$(docker_run) coverage run setup.py test
	$(docker_run) coverage run --append tks/tks.py
	$(docker_run) coverage report --fail-under=100 --show-missing

.PHONY: lint
lint: container
	$(docker_run) mypy .
	$(docker_run) flake8
	$(docker_run) black --check .
	$(docker_run) isort --check-only

.PHONY: format
format: container
	$(docker_run) black .
	$(docker_run) isort

.PHONY: container
container:
	docker build --build-arg UID="$${GITLAB_USER_ID:-$$UID}" --tag $(docker_tag) .

.PHONY: git-hooks
git-hooks: .git/hooks/pre-commit

.git/hooks/%: git-hooks/%.sh
	install --mode=700 $< $@

.PHONY: build
