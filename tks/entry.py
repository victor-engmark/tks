from datetime import datetime


class Entry:
    def __init__(self, start_datetime: datetime, end_datetime: datetime):
        self.start_datetime = start_datetime
        self.end_datetime = end_datetime
        self.duration = end_datetime - start_datetime
