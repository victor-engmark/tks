from datetime import datetime, timedelta
from unittest import TestCase

from .entry import Entry


class TestEntry(TestCase):
    def test_should_calculate_duration(self) -> None:
        start_datetime = datetime(2000, 1, 1, hour=8)
        end_datetime = datetime(2000, 1, 1, hour=16)
        entry = Entry(start_datetime, end_datetime)

        self.assertEqual(timedelta(hours=8), entry.duration)
