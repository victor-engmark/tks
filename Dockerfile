FROM python:3

WORKDIR /usr/src/app

ARG UID
RUN useradd \
    --create-home \
    --shell /bin/bash \
    --uid "$UID" \
    user

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

USER user
