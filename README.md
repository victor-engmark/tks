# TKS: Time Keeping Sucks

![status](https://gitlab.com/victor-engmark/tks/badges/master/pipeline.svg)
![coverage](https://gitlab.com/victor-engmark/tks/badges/master/coverage.svg)

Everyone knows that timekeeping sucks. TKS is designed to make it suck less. You can keep track of your time in a human-readable file, then run a script to import the data straight into your timekeeping system.

This is an in-process migration from [the original Perl-based project](https://github.com/shoptime/tks) to Python 3.

## Development

Requirements:

- Docker
- Python 3

Important commands:

- `make git-hooks` installs a Git pre-commit hook to lint the code. Once installed a `git commit` command will fail if linting fails. You can bypass the checks with `--no-verify` - this is useful when committing non-Python changes.
- `make test` tests and verifies coverage.
- `make lint` verifies code structure.
- `make format` formats the code and imports.

## Copyright & license

TKS is copyright (C) 2008 - 2019, Catalyst IT Ltd.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

## Contributors

TKS was originally written by Nigel McNie and Martyn Smith.
